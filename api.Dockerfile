# Utilisez une image de base appropriée
FROM python:3.10.13-alpine3.18

# Copiez le contenu du dossier app/ dans /app/
COPY ./back /app/
COPY ./back/requirements.txt /tmp/pip/

# Définissez le répertoire de travail
WORKDIR /app

RUN pip install -r /tmp/pip/requirements.txt

EXPOSE 5050

# Exécutez le fichier modules.sh
CMD ["python3","/app/app.py"]
