from datetime import datetime
import pandas as pd

from config import app_database, app_sql


def calculer_rang():
    # print("RANG JOUEURS")
    joueurs = app_database.execute_query(
        "SELECT * FROM classement ORDER BY points DESC"
    )

    for i in range(len(joueurs)):
        joueurs[i]["rang_joueur"] = i + 1

    query_update = app_sql.generate_update_clause("classement", joueurs[0])
    condition = f" WHERE username=%(username)s;"
    query = query_update + condition
    queries = [(query, joueur) for joueur in joueurs]
    app_database.execute_queries(queries)

def determiner_resultat(
        statut_match: str,
        score_a: int,
        score_b: int,
        score_prono_a: int,
        score_prono_b: int
    ) -> int:
    points = 0

    # On ne calcule pas les points si le match a commencé
    print(statut_match)
    if statut_match != "termine":
        print("not terminé")
        return points

    lst_elements = [statut_match, score_a, score_b, score_prono_a, score_prono_b]
    print(lst_elements)
    if any(element is None for element in lst_elements):
        return points

    score_a = int(score_a)
    score_b = int(score_b)
    print("calcul des points")
    # Equipe gagnante du match
    if score_a > score_b:
        equipe_gagnante = 'a'
    if score_a < score_b:
        equipe_gagnante = 'b'
    if score_a == score_b:
        equipe_gagnante = 'nul'

    # Equipe gagnate dans le prono
    if score_prono_a > score_prono_b:
        equipe_gagnante_prono = 'a'
    if score_prono_a < score_prono_b:
        equipe_gagnante_prono = 'b'
    if score_prono_a == score_prono_b:
        equipe_gagnante_prono = 'nul'

    # Check si la bonne équipe gagnante
    if equipe_gagnante == equipe_gagnante_prono:
        points = points + 5
    
    # bon score 2 équipes
    if (score_a == score_prono_a) and (score_b == score_prono_b):
        points = points + 10
        return points

    # bon score pour une des deux équipes
    if (score_a == score_prono_a) or (score_b == score_prono_b):
        points = points + 5
        return points
    
    return points

def calculer_points_par_prono():
    all_data = app_database.execute_query("""
        SELECT *
        FROM matchs ma 
        INNER JOIN pronostiques prono 
        ON ma.id_match = prono.id_match
        INNER JOIN ab_user
        ON ab_user.id = prono.id_joueur;
    """)
    all_data = pd.DataFrame(all_data)
    all_data = all_data[['id_pronostique', 'points', 'username', 'score_a', 'score_b', 'score_prono_a', 'score_prono_b', 'statut_match']]
    print(all_data.columns)

    all_data['points'] = list(map(determiner_resultat,
        all_data['statut_match'],
        all_data['score_a'],
        all_data['score_b'],
        all_data['score_prono_a'],
        all_data['score_prono_b'])
    )

    classements = all_data[['username', 'points']].groupby(by='username').sum().reset_index().to_dict('records')
    pronostiques = all_data[['id_pronostique', 'points']].to_dict('records')

    # Formation des queries pour update la donnée
    query_prono = """
        UPDATE pronostiques 
        SET points = %(points)s 
        WHERE id_pronostique=%(id_pronostique)s;
        """
    queries = [(query_prono, pronostique) for pronostique in pronostiques]
    app_database.execute_queries(queries)

    query_classement = """
        UPDATE classement 
        SET points = %(points)s 
        WHERE username=%(username)s;
        """
    queries = [(query_classement, classement) for classement in classements]
    app_database.execute_queries(queries)


