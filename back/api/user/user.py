from werkzeug.security import generate_password_hash
from flask_appbuilder.api import BaseApi, expose
from flask import request, g
from flask_mail import Message
from datetime import datetime
import pandas as pd

from config import app_database, app_sql, mail


class User(BaseApi):

    @expose("/enregistrement", methods=["POST"])
    def enregistrement(self):
        data = request.get_json()
        if set(["username", "password", "mail"]) == set(data.keys()):
            mail_split = data["mail"].split("@")
            if len(mail_split) == 2:
                # if not mail_split[1] == 'finances.gouv.fr':
                #    return self.response_400(message='Format de mail incorrect')

                check_mail = app_database.execute_query(
                    f"SELECT * FROM ab_user WHERE email='{mail_split[1]}'"
                )
                check_username = app_database.execute_query(
                    f"SELECT * FROM ab_user WHERE username='{data['username']}'"
                )

                if len(check_mail) != 0:
                    return self.response_400(message="mail déjà utilisé")

                if len(check_username) != 0:
                    return self.response_400(message="identifiant déjà utilisé")

                heure_creation = datetime.now()

                # Ajout d'un joueur
                max_id_user = app_database.execute_query(
                    "SELECT MAX(id) as id FROM ab_user;"
                )
                app_database.execute_query(
                    f"""INSERT INTO ab_user (id, first_name, last_name, username, password, active, email, created_on)
                                           VALUES ('{max_id_user[0]['id'] + 1}','{data['username']}', 'bercy', '{data['username']}','{generate_password_hash(data['password'])}', {True}, '{data['mail']}', '{heure_creation}');"""
                )
                new_user_id = app_database.execute_query(
                    f"SELECT id FROM ab_user WHERE username='{data['username']}'"
                )

                if len(new_user_id) == 0:
                    return self.response_500(
                        message="Erreur lors de la création de l'utilisateur"
                    )

                # Ajout d'un role au joueur
                max_id_user_role = app_database.execute_query(
                    "SELECT MAX(id) as id FROM ab_user_role;"
                )
                app_database.execute_query(
                    f"""INSERT INTO ab_user_role (id, user_id, role_id) VALUES ({max_id_user_role[0]['id'] + 1}, {new_user_id[0]['id']}, 2);"""
                )

                # Ajout du joueur au classement
                max_id_user_classement = app_database.execute_query(
                    "SELECT MAX(id_classement) as id FROM classement;"
                )
                if len(max_id_user_classement) == 0:
                    id_user_classement = 1
                else:
                    id_user_classement = max_id_user_classement[0]['id'] + 1
                app_database.execute_query(
                    f"""INSERT INTO classement (id_classement, username, points) VALUES ({id_user_classement}, '{data['username']}', 0);"""
                )

                """ msg = Message(
                    subject="Prono Bercy - Confirmation d'inscription",
                    html="Ce mail confirme la création de votre compte sur Prono Bercy!",
                    recipients=[data["mail"]],
                )
                mail.send(msg) """

                return self.response(201, message="Nouvel utilisateur créé")

            else:
                return self.response_400(message="Format de mail incorrect")
        else:
            return self.response_400(data)

    @expose("/classement", methods=["GET"])
    def classement(self):
        joueurs = app_database.execute_query(
            "SELECT username, points, rang_joueur FROM classement ORDER BY points DESC"
        )
        rows = []
        for joueur in joueurs:
            rows.append([joueur['rang_joueur'], joueur['username'], joueur['points']])

        return self.response(code=200, results=rows)
