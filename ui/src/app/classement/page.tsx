"use client";

import { Table } from "@codegouvfr/react-dsfr/Table";
import { useEffect, useState } from "react";

export default function Classement() {
  const headers = ["Rang", "Joueur", "Points"];
  const [classement, setClassement] = useState([]);
  const fake_data = [
    [1, "Frosch", 100],
    [2, "Toga", 20],
    [3, "Acnologia", 2]
  ];
  const last_maj = "22/06/2024 17h30";

  useEffect(() => {
    const fetchClassement = async () => {
      try {
        const response = await fetch(
          process.env.NEXT_PUBLIC_API_URL + "/user/classement", {
          method: "GET",
          headers: {
            "Content-Type": "Application/json",
          }
        });

        if (!response.ok) {
          throw new Error("Failed to fetch fiche data");
        }
        const data_classement = await response.json();
        setClassement(data_classement['results']);
        console.log("ici");
        console.log(data_classement);
      } catch (error) {
        console.error("Error fetching classement:", error);
      }
    };
    fetchClassement();
  }, []);

  return (
    <>
      <h1>Classement général</h1>
      <Table
        headers={headers}
        data={classement}
      />
    </>
  );
}
