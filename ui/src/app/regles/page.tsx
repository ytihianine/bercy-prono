export default function Regles() {
  return (
    <>
      <h1>Calcul des points</h1>
      <p>
        Si vous avez trouvé le bon résultat du match: vous gagner 10 points.
      </p>
      <p>
        Si vous avez donné le bon nombre de but pour une des deux équipes: vous
        gagner 5 points.
      </p>
      <p>
        Si vous avez donné la bonne équipe victorieuse ou le match nul: vous
        gagner 5 points.
      </p>
      <p>Sinon, dommage mais c'est 0 point.</p>
      <p>Exemple 1: France 1 - 2 Belgique ---- pronosique: France 1 - 2 Belgique</p>
      <ul>
        <li>Vous gagnez 10 points pour le bon résultat</li>
        <li>Vous gagnez 5 point pour avoir donné la bonne équipe victorieuse.</li>
      </ul>
      <p>Total: 15 points gagnés</p>
      <p>Exemple 2: France 1 -  1 Autriche - pronostique: France 1 - 1 Autriche</p>
      <ul>
        <li>Vous gagnez 10 points pour le bon résultat</li>
        <li>Vous gagnez 5 point pour avoir donné la bonne équipe victorieuse.</li>
      </ul>
      <p>Total: 15 points gagnés.</p>
      <p>Exemple 3: France 1 - 1 Espagne, la France gagne aux tirs au but - pronostique: France 2 - 1 Espagne</p>
      <ul>
        <li>Vous gagnez 5 points pour le bon nombre de but pour l'espagne</li>
      </ul>
      <p>Total: 5 points gagnés.</p>
      <p>Le gagnant issu des tirs au but n'est pas pris en compte.</p>
    </>
  );
}
